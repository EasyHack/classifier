# Copyright 2015 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Simple image classification with Inception.

Run image classification with Inception trained on ImageNet 2012 Challenge data
set.

This program creates a graph from a saved GraphDef protocol buffer,
and runs inference on an input JPEG image. It outputs human readable
strings of the top 5 predictions along with their probabilities.

Change the --image_file argument to any jpg image to compute a
classification of that image.

Please see the tutorial and website for a detailed description of how
to use this script to perform image recognition.

https://tensorflow.org/tutorials/image_recognition/
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os.path
import re
import sys
import tarfile
from datetime import datetime
import time

import numpy as np
from six.moves import urllib
import tensorflow as tf


MODEL_DIR = '/tmp/imagenet'

ALL = ["azul", "amarillo", "verde", "electronica", "aceite", "medicamento"]

AZUL = ["carton", "packet"]
AMARILLO = ["water bottle", "bottlecap"]
VERDE = ["water bottle", "pop bottle, soda bottle", "beer bottle", "vase", "beer glass"]
ELECTRONICA = ["monitor", "loudspeaker, speaker, speaker unit, loudspeaker system, speaker system"]
ACEITE = ["oil filter", "water bottle", "pop bottle, soda bottle", "ashcan, trash can, garbage can, wastebin, ash bin, ash-bin, ashbin, dustbin, trash barrel, trash bin"]
MEDICAMENTO = ["medicine chest, medicine cabinet", "pill bottle"]



class NodeLookup(object):
  """Converts integer node ID's to human readable labels."""

  def __init__(self,
               label_lookup_path=None,
               uid_lookup_path=None):
    if not label_lookup_path:
      label_lookup_path = os.path.join(
          MODEL_DIR, 'imagenet_2012_challenge_label_map_proto.pbtxt')
    if not uid_lookup_path:
      uid_lookup_path = os.path.join(
          MODEL_DIR, 'imagenet_synset_to_human_label_map.txt')
    self.node_lookup = self.load(label_lookup_path, uid_lookup_path)

  def load(self, label_lookup_path, uid_lookup_path):
    if not tf.gfile.Exists(uid_lookup_path):
      tf.logging.fatal('File does not exist %s', uid_lookup_path)
    if not tf.gfile.Exists(label_lookup_path):
      tf.logging.fatal('File does not exist %s', label_lookup_path)

    # Loads mapping from string UID to human-readable string
    proto_as_ascii_lines = tf.gfile.GFile(uid_lookup_path).readlines()
    uid_to_human = {}
    p = re.compile(r'[n\d]*[ \S,]*')
    for line in proto_as_ascii_lines:
      parsed_items = p.findall(line)
      uid = parsed_items[0]
      human_string = parsed_items[2]
      uid_to_human[uid] = human_string

    # Loads mapping from string UID to integer node ID.
    node_id_to_uid = {}
    proto_as_ascii = tf.gfile.GFile(label_lookup_path).readlines()
    for line in proto_as_ascii:
      if line.startswith('  target_class:'):
        target_class = int(line.split(': ')[1])
      if line.startswith('  target_class_string:'):
        target_class_string = line.split(': ')[1]
        node_id_to_uid[target_class] = target_class_string[1:-2]

    # Loads the final mapping of integer node ID to human-readable string
    node_id_to_name = {}
    for key, val in node_id_to_uid.items():
      if val not in uid_to_human:
        tf.logging.fatal('Failed to locate: %s', val)
      name = uid_to_human[val]
      node_id_to_name[key] = name

    return node_id_to_name

  def id_to_string(self, node_id):
    if node_id not in self.node_lookup:
      return ''
    return self.node_lookup[node_id]


def create_graph():
    with tf.gfile.FastGFile(os.path.join(
                MODEL_DIR, 'classify_image_graph_def.pb'), 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        _ = tf.import_graph_def(graph_def, name='')


class Service():
    
    def __init__(self, k=10):
        self.k = k;
        # Creates graph from saved GraphDef.
        create_graph()

    def classify(self, imagepath):
        # Get image data.
        if not tf.gfile.Exists(imagepath):
            tf.logging.fatal('File does not exist %s', imagepath)
        image_data = tf.gfile.FastGFile(imagepath, 'rb').read()

        with tf.Session() as sess:
            softmax_tensor = sess.graph.get_tensor_by_name('softmax:0')
            predictions = sess.run(softmax_tensor,
                           {'DecodeJpeg/contents:0': image_data})
            predictions = np.squeeze(predictions)

            # Creates node ID --> English string lookup.
            node_lookup = NodeLookup()

            top_k = predictions.argsort()[-self.k:][::-1]
            tags_name = []
            tags_score = []
            for node_id in top_k:
                human_string = node_lookup.id_to_string(node_id)
                score = predictions[node_id]
                tags_name.append(human_string)
                tags_score.append(score)
                print('%s (score = %.5f)' % (human_string, score))
            return tags_name, tags_score, self.get_bin(tags_name, tags_score)

    def get_bin(self, tags_name, tags_score):
        azul = [tag for tag in AZUL if tag in tags_name]
        amarillo = [tag for tag in AMARILLO if tag in tags_name]
        verde = [tag for tag in VERDE if tag in tags_name]
        electronica = [tag for tag in ELECTRONICA if tag in tags_name]
        aceite = [tag for tag in ACEITE if tag in tags_name]
        medicamento = [tag for tag in MEDICAMENTO if tag in tags_name]
        
        all = [len(azul), len(amarillo), len(verde), len(electronica), len(aceite), len(medicamento)]
        print(all)
        max_bin = max(all)
        max_bin_pos = [i for i, j in enumerate(all) if j == max_bin]
        if max_bin > 0:
            return ALL[max_bin_pos[0]]
        else:
            return "gris"
