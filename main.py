#!/usr/bin/env python
# encoding=utf-8

"""

Servidor HTTP simple en Python.

"""

import json
import logging
import re
import SocketServer
from sys import argv
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import Image
import StringIO
from tempfile import SpooledTemporaryFile
import json
from service import *


SERVICE = Service(k=8)


class MyHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        file = open("static/index.html")
        self.wfile.write(file.read())

    def do_POST(self):
        self.send_response(200)
        content_len = int(self.headers.getheader('content-length', 0))
        line = self.rfile.readline()
        content_len -= len(line)
        first_len = len(line)
        line = self.rfile.readline()
        content_len -= len(line)
        line = self.rfile.readline()
        content_len -= len(line)
        line = self.rfile.readline()
        content_len -= len(line)
        json_str = self.rfile.read(content_len)
        print(content_len - first_len)
        print("Primeros 10 ", json_str[:10])
        #im = Image.open(StringIO.StringIO(json_str))
        #print im.format, im.size, im.mode
        
        imagepath = 'sample.jpg'
        open(imagepath, 'wb').write(json_str)
        tags_name, tags_score, bin = SERVICE.classify(imagepath)
        self.write_results_json(tags_name, tags_score, bin)

    def write_results(self, tags_name, tags_score, bin):
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        file = open("static/result.html")
        html = file.read()
        tags = ""
        for i in range(0, len(tags_name)):
            tags += tags_name[i] + " (" + str(tags_score[i]) + ")<br>"
        self.wfile.write(html % {"tags": tags, "bin": bin})
    
    def write_results_json(self, tags_name, tags_score, bin):
        self.send_header('Content-type', 'text/json')
        self.end_headers()
        tags_score = [float(s) for s in tags_score]
        json_str = json.dumps({"tags" : tags_name, "score" : tags_score, "bin" : bin})
        self.wfile.write(json_str)


def run(server_class=HTTPServer, handler_class=MyHandler, port=8080):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting server...'
    httpd.serve_forever()


if __name__ == "__main__":
    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
